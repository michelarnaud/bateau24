/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.cpp
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include <main.h>
#include <stdio.h>
#include <string.h>
#include <cstring>
#include "cJSON.h"
#include <cstdlib>

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
UART_HandleTypeDef huart4;
UART_HandleTypeDef huart1;
UART_HandleTypeDef huart3;

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_UART4_Init(void);
static void MX_USART1_UART_Init(void);
static void MX_USART3_UART_Init(void);
/* USER CODE BEGIN PFP */

// Déclaration des variables globales :
uint8_t datar;
uint8_t nrecu;

uint8_t rxdata[100];
uint8_t rxgps[100];
uint8_t payloadgps[100];

char msg[20];
char rxchardata[100];
char payload[120];

uint8_t rxindexdata;
uint8_t rxindexgps;
uint8_t rxigps;
uint8_t sizegps;
uint8_t sizedata;

uint8_t newmesuredata = 0;
uint8_t newmesuregps = 0;

uint8_t gpsr;

uint8_t gps[13] = {'A','T','+','C','G','P','S','I','N','F','O','\r','\n'};
uint8_t RetourLigne[2] = {'\r','\n'};

uint8_t cmqttstart[16]={'A','T','+','C','M','Q','T','T','S','T','A','R','T','\r','\n',0};//"AT+CMQTTSTART\r\n";
uint8_t cmqttaccq[24]={'A','T','+','C','M','Q','T','T','A','C','C','Q','=','0',',','"','m','q','t','t','"','\r','\n',0};//"AT+CMQTTACCQ=0,"mqtt"\r\n";
uint8_t cmqttconnect[73]={'A','T','+','C','M','Q','T','T','C','O','N','N','E','C','T','=','0',',','"','t','c','p',':','/','/','1','9','3','.','2','5','3','.','2','2','2','.','1','6','2',':','2','0','8','3','"',',','6','0',',','1',',','"','m','q','t','t','"',',','"','s','n','i','r','p','a','s','s','"','\r','\n',0};//"AT+CMQTTCONNECT=0,"tcp://193.253.222.162:2083",60,1,"mqtt","snirpass"\r\n";
uint8_t cmqtttopic[21]={'A','T','+','C','M','Q','T','T','T','O','P','I','C','=','0',',','1','2','\r','\n',0};//"AT+CMQTTTOPIC=0,12\r\n";
uint8_t topic_projet[13]={'t','o','p','i','c','/','p','r','o','j','e','t',0};//"topic/projet";
uint8_t cmqttpayload[23]={'A','T','+','C','M','Q','T','T','P','A','Y','L','O','A','D','=','0',',','6','7','\r','\n',0};//"AT+CMQTTPAYLOAD=0,59\r\n";
uint8_t message_payload[104]={'4','6','4','0','.','5','2','5','8','2','7',',','N',',','0','0','1','2','4','.','4','2','0','0','2','4',',','W',',','2','2','0','5','2','3',',','1','1','3','5','0','0','.','0',',','7','1','.','1',',','0','.','0',',','1','6','5','.','8',',','0',',','0',',','0',',','0',0};//"4640.525827,N,00124.420024,W,030522,163000.0,71.1,0.0,165.8";
uint8_t cmqttpub[21]={'A','T','+','C','M','Q','T','T','P','U','B','=','0',',','0',',','6','0','\r','\n',0};//"AT+CMQTTPUB=0,0,60\r\n";

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart){

//Fonction de callback pour récupérer les infos des trames GPS :
	if(huart->Instance == USART1) {
			if(gpsr == 10){
				rxgps[rxindexgps] = 10;
				newmesuregps = 1;
			} else {
				rxgps[rxindexgps] = gpsr;
				rxindexgps++;
				if (rxindexgps>99){
					rxindexgps=0;
				}
			}
			HAL_UART_Receive_IT(&huart1,&gpsr,1);
		}

	//-----------------------------------------------------//

//Fonction de callback pour récupérer les infos des trames JSON :
	if(huart->Instance == USART3){
		if(datar == 13){
			nrecu = rxindexdata;
			rxindexdata = 0;
			newmesuredata = 1;
		} else {
			rxdata[rxindexdata] = datar;
			rxindexdata++;
		}
		HAL_UART_Receive_IT(&huart3,&datar,1);
	}
}

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_UART4_Init();
  MX_USART1_UART_Init();
  MX_USART3_UART_Init();
  /* USER CODE BEGIN 2 */

  //Activation des récéptions en interruption :
  HAL_UART_Receive_IT(&huart1,&gpsr,1);
  HAL_UART_Receive_IT(&huart3,&datar,1);

  //Etablissement de la connexion avec le serveur MQTT et envoie du message dans le topic "projet" :
 /* HAL_UART_Transmit(&huart1,cmqttstart,sizeof(cmqttstart),1000);
  HAL_Delay(1000);
  HAL_UART_Transmit(&huart1,cmqttaccq,23,1000);
  HAL_Delay(1000);
  HAL_UART_Transmit(&huart1,cmqttconnect,72,5000);
  HAL_Delay(1000);
  HAL_UART_Transmit(&huart1,cmqtttopic,20,3000);
  HAL_Delay(1000);
  HAL_UART_Transmit(&huart1,topic_projet,12,1000);
  HAL_Delay(1000);
  HAL_UART_Transmit(&huart1,cmqttpayload,22,1000);
  HAL_Delay(1000);
  HAL_UART_Transmit(&huart1,message_payload, sizeof(message_payload),5000);
  HAL_Delay(1000);
  HAL_UART_Transmit(&huart1,cmqttpub,20,1000);*/


  /* USER CODE END 2 */

  //On interroge le GPS :
  HAL_UART_Transmit(&huart1,gps,sizeof(gps),HAL_MAX_DELAY);

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
	 {
	     //Algorithme qui permet de lire dans le tableau 'rxigps' et supprimer les entêtes indésirables 'AT+CGPSINFO', '+CGPSINFO :' et 'OK' :
	     if(newmesuregps == 1){
	  	    rxigps=rxindexgps-29;
	      for (int i=0;i<rxigps;i++){
	  		payloadgps[i]=rxgps[i+24];
	   	 }

	  	 newmesuregps = 0;
	  	 sizegps=rxigps;
	  	 rxindexgps=0;
	     }

	     if(newmesuredata == 1){
	  	    for (int i=1;i<nrecu;i++){
	  		 rxchardata[i-1]=rxdata[i];
	  	 }

	  	 //On commence par parser les données JSON reçues depuis l'USART 3 :
	  	 cJSON* json_data = cJSON_Parse(rxchardata);

	  	 //On récupère les valeurs des différentes clés :
	     cJSON* capMagn = cJSON_GetObjectItem(json_data, "capMagn");
	  	 cJSON* vitVent = cJSON_GetObjectItem(json_data, "vitVent");
	     cJSON* dirVent = cJSON_GetObjectItem(json_data, "dirVent");
	  	 cJSON* angleBarre = cJSON_GetObjectItem(json_data, "angleBarre");

	  	 //On effectue une concaténation du GPS et des données issue du JSON :
	  	 sprintf(msg,"%d,%d,%d,%d", capMagn->valueint, vitVent->valueint, dirVent->valueint, angleBarre->valueint);
	  	 sprintf(payload,"%s,%s",payloadgps,msg);
	     sizedata=sizegps+sizeof(msg)+1;

	  	 //On envoie la trame sur le port série USART 3 :
	  	 HAL_UART_Transmit(&huart3,(const uint8_t*)payload,sizedata,HAL_MAX_DELAY);
	  	 HAL_UART_Transmit(&huart3,RetourLigne,2, HAL_MAX_DELAY);

	     //On libère de la mémoire :
	  	 rxindexdata = 0;
	  	 cJSON_Delete(json_data);
	  	 free(json_data);
	  	 newmesuredata=0;
	  	 }

	  	 //On actualise toutes les secondes :
	     HAL_UART_Transmit(&huart1,gps,sizeof(gps),HAL_MAX_DELAY);
	  	 HAL_Delay(1000);


    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

//}
/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 1;
  RCC_OscInitStruct.PLL.PLLN = 10;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV7;
  RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_4) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief UART4 Initialization Function
  * @param None
  * @retval None
  */
static void MX_UART4_Init(void)
{

  /* USER CODE BEGIN UART4_Init 0 */

  /* USER CODE END UART4_Init 0 */

  /* USER CODE BEGIN UART4_Init 1 */

  /* USER CODE END UART4_Init 1 */
  huart4.Instance = UART4;
  huart4.Init.BaudRate = 115200;
  huart4.Init.WordLength = UART_WORDLENGTH_8B;
  huart4.Init.StopBits = UART_STOPBITS_1;
  huart4.Init.Parity = UART_PARITY_NONE;
  huart4.Init.Mode = UART_MODE_TX_RX;
  huart4.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart4.Init.OverSampling = UART_OVERSAMPLING_16;
  huart4.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart4.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart4) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN UART4_Init 2 */

  /* USER CODE END UART4_Init 2 */

}

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 115200;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  huart1.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart1.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART1_Init 2 */

  /* USER CODE END USART1_Init 2 */

}

/**
  * @brief USART3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART3_UART_Init(void)
{

  /* USER CODE BEGIN USART3_Init 0 */

  /* USER CODE END USART3_Init 0 */

  /* USER CODE BEGIN USART3_Init 1 */

  /* USER CODE END USART3_Init 1 */
  huart3.Instance = USART3;
  huart3.Init.BaudRate = 115200;
  huart3.Init.WordLength = UART_WORDLENGTH_8B;
  huart3.Init.StopBits = UART_STOPBITS_1;
  huart3.Init.Parity = UART_PARITY_NONE;
  huart3.Init.Mode = UART_MODE_TX_RX;
  huart3.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart3.Init.OverSampling = UART_OVERSAMPLING_16;
  huart3.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart3.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart3) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART3_Init 2 */

  /* USER CODE END USART3_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};
/* USER CODE BEGIN MX_GPIO_Init_1 */
/* USER CODE END MX_GPIO_Init_1 */

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, LD3_Pin|LD4_Pin|LD5_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : B1_Pin */
  GPIO_InitStruct.Pin = B1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(B1_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : USART_TX_Pin USART_RX_Pin */
  GPIO_InitStruct.Pin = USART_TX_Pin|USART_RX_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.Alternate = GPIO_AF7_USART2;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : LD2_Pin */
  GPIO_InitStruct.Pin = LD2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(LD2_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : LD3_Pin LD4_Pin LD5_Pin */
  GPIO_InitStruct.Pin = LD3_Pin|LD4_Pin|LD5_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

/* USER CODE BEGIN MX_GPIO_Init_2 */
/* USER CODE END MX_GPIO_Init_2 */
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
