//const http = require('http');
const mqtt = require('mqtt');
const mysql = require('mysql');
const reglesNavigation = require('./regles_navigation.js');

//-----------------------------------------------------------------------------
//
//                              Fonctions Mqtt
//
//-----------------------------------------------------------------------------

const mqttLocal = 'mqtt://localhost';
const topicLocal = "topic";

const mqttApp = 'mqtt://192.168.10.189';
const topicApp = "App";

const mqttAppSimulation = 'mqtt://192.168.10.164';
const topicAppSimulation = "simulation";
//-----------------------------------------------------------------------------

function notifMqtt(message) {
    const client = mqtt.connect(mqttApp);

    client.on('connect', () => {
        client.publish(topicApp, message, (err) => {
            if (err) {
                console.error('Erreur lors de la publication du message', err);
            } else {
                client.end(); // Fermer la connexion après l'envoi du message
            }
        });
    });
/*
    client.on('error', (error) => {
        console.error('Erreur MQTT:', error);
    });
*/
}

function notifSimulation(message) {
    const client = mqtt.connect(mqttAppSimulation);

    client.on('connect', () => {
        client.publish(topicAppSimulation, message, (err) => {
            if (err) {
                console.error('Erreur lors de la publication du message', err);
            } else {
                client.end(); // Fermer la connexion après l'envoi du message
            }
        });
    });
/*
    client.on('error', (error) => {
        console.error('Erreur MQTT:', error);
    });
*/
}

function messageMqtt(){
    const client = mqtt.connect(mqttLocal);

    client.on('connect', () => {
    console.log(`Connecté au broker MQTT sur ${mqttLocal}`);
    client.subscribe(topicLocal); // S'abonne au sujet "topic"
    });
    
    client.on('message', (topicLocal, message) => {
    let messageString = message.toString();
    ajoutDonneesMysql(messageString);
    
    });
}

//-----------------------------------------------------------------------------
//
//                              Fonctions Règles navigations
//
//-----------------------------------------------------------------------------

function reglesNavigationFonction(bateau1, bateau2, vent){
    let collision = false;
    for(let i = 0 ; i<20 ; i++){
        let futurPositionBateau1 = reglesNavigation.futurPosition(bateau1, i);
        let futurPositionBateau2 = reglesNavigation.futurPosition(bateau2, i);
        if(reglesNavigation.verificationColision(futurPositionBateau1, futurPositionBateau2, vent)){
            //console.log('\x1b[31m%s\x1b[0m', 'DANGER ' + distanceLatitude + " " + distanceLongitude);
            collision = true;
        }
    }
    if(collision){
        let capCorrigees = reglesNavigation.correctionCap(bateau1, bateau2, vent);
        return [true, capCorrigees[0], capCorrigees[1]];
    } else{
        return [false, 0, 0];
    }
}


//-----------------------------------------------------------------------------
//
//                              Fonctions Mysql
//
//-----------------------------------------------------------------------------

    const connection = mysql.createConnection({
        host: '127.0.0.1',
        user: 'root',
        password: 'snir',
        database: 'projet_bateau'
    });

    const port = 3001;

//-----------------------------------------------------------------------------
function ajoutDonneesMysql(message){
    
    const newMessage = message.split(":");
    const position1 = newMessage[0].split(",");
    const position2 = newMessage[1].split(",");
    const vent = newMessage[2];
    const dateActuelle = new Date();

    // Exécuter la commande INSERT INTO
    const sql = 'INSERT INTO bateau1 (time, latitude, longitude, cap, vitesse, capR) VALUES (?, ?, ?, ?, ?, ?)';
    let time = dateActuelle.getHours() + ":" + dateActuelle.getMinutes() + ":" + dateActuelle.getSeconds();
    let latitude = parseFloat(position1[0]);
    let longitude = parseFloat(position1[1]);
    let cap = parseInt(position1[2]);
    let vitesse = parseInt(position1[3]);
    let capR = parseInt(position1[4]);
    let values = [time.toString(), latitude, longitude, cap, vitesse, capR]
    
    connection.query(sql, values, (err, result) => {
        if (err) {
        console.error('Erreur lors de l\'exécution de la commande INSERT INTO :', err);
        } else {
        //console.log('Ajout de la position ' , message , ' dans la base de données');
        }
    });

    const sql2 = 'INSERT INTO bateau2 (time, latitude, longitude, cap, vitesse, capR) VALUES (?, ?, ?, ?, ?, ?)';
    let time2 = dateActuelle.getHours() + ":" + dateActuelle.getMinutes() + ":" + dateActuelle.getSeconds();
    let latitude2 = parseFloat(position2[0]);
    let longitude2 = parseFloat(position2[1]);
    let cap2 = parseInt(position2[2]);
    let vitesse2 = parseInt(position2[3]);
    let capR2 = parseInt(position2[4]);
    let values2 = [time2.toString(), latitude2, longitude2, cap2, vitesse2, capR2]

    connection.query(sql2, values2, (err, result) => {
        if (err) {
        console.error('Erreur lors de l\'exécution de la commande INSERT INTO :', err);
        } else {
        //console.log('Ajout de la position ' , message , ' dans la base de données');
        }
    });

    const sql3 = 'INSERT INTO vent (vent) VALUES (?)';
    let values3 = [vent]
    connection.query(sql3, values3, (err, result) => {
        if (err) {
        console.error('Erreur lors de l\'exécution de la commande INSERT INTO :', err);
        } else {
        //console.log('Ajout de la position ' , message , ' dans la base de données');
        }
    });
      
    // Gérer la fermeture de la connexion lorsque le serveur est arrêté
    process.on('SIGINT', () => {
    connection.end();
    process.exit();
    });
}

async function positionBateau() {
    const position = {
        bateau1: [0, 0],
        bateau2: [0, 0],
        impacte: false,
        vent: 0
    };

    return new Promise((resolve, reject) => {
        connection.query('SELECT time, latitude, longitude, cap, vitesse, capR FROM bateau1 ORDER BY id DESC LIMIT 1', (error, results, fields) => {
            if (error) {
                console.error(error);
                reject(error);
            } else {
                const lastCoordinates = results[0];
                if (lastCoordinates !== undefined && lastCoordinates !== null) {
                    position.bateau1 = [lastCoordinates.latitude, lastCoordinates.longitude, lastCoordinates.cap, lastCoordinates.vitesse, lastCoordinates.capR];
                } else {
                    console.log("Erreur : la table bateau1 est vide");
                }
            }

            connection.query('SELECT time, latitude, longitude, cap, vitesse, capR FROM bateau2 ORDER BY id DESC LIMIT 1', (error, results, fields) => {
                if (error) {
                    console.error(error);
                    reject(error);
                } else {
                    const lastCoordinates = results[0];
                    if (lastCoordinates !== undefined && lastCoordinates !== null) {
                        position.bateau2 = [lastCoordinates.latitude, lastCoordinates.longitude, lastCoordinates.cap, lastCoordinates.vitesse, lastCoordinates.capR];
                    } else {
                        console.log("Erreur : la table bateau2 est vide");
                    }
                }
                connection.query('SELECT vent FROM vent ORDER BY id DESC LIMIT 1', (error, results, fields) => {
                    if (error) {
                        console.error(error);
                        reject(error);
                    } else {
                        const vent = results[0];
                        if (vent !== undefined && vent !== null) {
                            position.vent = vent.vent;
                        } else {
                            console.log("Erreur : la table vent est vide");
                        }
                    }
                    position.impacte = reglesNavigationFonction(position.bateau1, position.bateau2, position.vent);
                    // Résoudre la promesse avec les positions correctes une fois que les requêtes sont terminées
                    resolve(position);
                });
            });
        });
    });
}
    

module.exports = {
    messageMqtt,
    positionBateau,
    notifMqtt,
    notifSimulation,
};



