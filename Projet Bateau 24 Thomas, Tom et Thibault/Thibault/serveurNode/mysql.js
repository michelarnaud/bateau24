const mysql = require('mysql');

const connection = mysql.createConnection({
  host: '127.0.0.1',
  user: 'root',
  password: 'snir',
  // Notez que nous n'incluons pas le nom de la base de données ici
  // car nous voulons vérifier ou créer la base de données plus tard.
});

function InitialisationDatabase(oui) {
  connection.connect((err) => {
    if (err) {
      console.error('Erreur de connexion à la base de données :', err.message);
      return;
    }

    console.log('Connecté à la base de données.');

    // Vérifie si la base de données existe
    connection.query('CREATE DATABASE IF NOT EXISTS projet_bateau', (err, result) => {
      if (err) {
        console.error('Erreur lors de la création de la base de données :', err.message);
        connection.end(); // Terminer la connexion en cas d'erreur
      } else {
        console.log('Base de données vérifiée ou créée avec succès.');

        // Sélectionne la base de données nouvellement créée
        connection.query('USE projet_bateau', (err) => {
          if (err) {
            console.error('Erreur lors de la sélection de la base de données :', err.message);
            connection.end();// Terminer la connexion en cas d'erreur
          } else {
            console.log('Base de données sélectionnée avec succès.');

            // Crée la table 'data'
            const createTableQuery = `
              CREATE TABLE IF NOT EXISTS bateau1 (
                id INT AUTO_INCREMENT PRIMARY KEY,
                time VARCHAR(255) NOT NULL,
                latitude DECIMAL(8,5) NOT NULL,
                longitude DECIMAL(8,5) NOT NULL,
                cap INT(3),
                vitesse INT(2),
                capR INT(3)
              )
            `;

            connection.query(createTableQuery, (err, result) => {
              if (err) {
                console.error('Erreur lors de la création de la table :', err.message);
              } else {
                console.log('Table "bateau1" créée avec succès.');
              }

              // Termine la connexion après avoir vérifié ou créé la table
            });
            const createTableQuery2 = `
              CREATE TABLE IF NOT EXISTS bateau2 (
                id INT AUTO_INCREMENT PRIMARY KEY,
                time VARCHAR(255) NOT NULL,
                latitude DECIMAL(8,5) NOT NULL,
                longitude DECIMAL(8,5) NOT NULL,
                cap INT(3),
                vitesse INT(2),
                capR INT(3)
              )
            `;
            connection.query(createTableQuery2, (err, result) => {
              if (err) {
                console.error('Erreur lors de la création de la table :', err.message);
              } else {
                console.log('Table "bateau2" créée avec succès.');
              }

              // Termine la connexion après avoir vérifié ou créé la table
            });
            const createTableQuery3 = `
            CREATE TABLE IF NOT EXISTS vent (
              id INT AUTO_INCREMENT PRIMARY KEY,
              vent INT(3) NOT NULL
            )
          `;
          connection.query(createTableQuery3, (err, result) => {
            if (err) {
              console.error('Erreur lors de la création de la table :', err.message);
            } else {
              console.log('Table "vent" créée avec succès.');
            }

            // Termine la connexion après avoir vérifié ou créé la table
            connection.end();
          });
          }
        });
      }
    });
  });
}

function AjoutData(){
  
}

// Utilisation des fonctions
InitialisationDatabase();
