const express = require('express');
const cors = require('cors'); // Importez le module cors
const bodyParser = require('body-parser'); // Import de body-parser
const app = express();
const port = 3001;
const fonction = require('./fonction.js');
var verificationDoubleColission = false;
var capModif = [0,0];
// ---------------------------------------------------------------------------------
//
//                              Initialisation du serveur + sécurité corse
//
// ---------------------------------------------------------------------------------

// Utilisez le middleware cors pour activer CORS
app.use(cors());
app.use(bodyParser.json());

//enable CORS for request verbs
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  res.header("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, OPTIONS");
  next();
});

app.listen(port, () => {
  console.log(`Serveur Node.js écoutant sur http://localhost:${port}`);
});

const basePosition = [46.491880220982, -1.7854207668632815];

// ---------------------------------------------------------------------------------
//
//                              Code du serveur
//
// ---------------------------------------------------------------------------------


fonction.messageMqtt();

app.get('/api/message', async (req, res) => {

  const position = await fonction.positionBateau();
  //bateau1, bateau2, vent, impacte
  //position.impacte[0] = true;
  capModif[0] = position.bateau1[2];
  capModif[1] = position.bateau2[2];
  if (position.impacte[0] == true) {
    if(verificationDoubleColission == false){
      let nb_rand = Math.random();
      var randomNumber = Math.round(nb_rand); // Génère 0 ou 1
      if (randomNumber == 1) {
        var cap = Math.floor(Math.random() * 360); // Génère un nombre entre 0 et 359
        capModif[0] = cap;
        capModif[1] = position.bateau2[2];
      } else {
        var cap = Math.floor(Math.random() * 360); // Génère un nombre entre 0 et 359
        capModif[0] = position.bateau1[2];
        capModif[1] = cap;
      }
      fonction.notifMqtt(`${position.bateau1[0]},${position.bateau1[1]},${position.bateau1[2]},${position.bateau1[3]}:${position.bateau2[0]},${position.bateau2[1]},${position.bateau2[2]},${position.bateau2[3]}:${position.impacte[0]},${capModif[0]},${capModif[1]}:${position.vent}`);
      //console.log(`position => ${position.bateau1[0]},${position.bateau1[1]},${position.bateau1[2]},${position.bateau1[3]}:${position.bateau2[0]},${position.bateau2[1]},${position.bateau2[2]},${position.bateau2[3]}:${position.vent}:${position.impacte[0]}`);
      //console.log(`position => ${position.vent}:${capModif[0]}:${capModif[1]}`);
      verificationDoubleColission = true;
    } else{
      console.log("Position déjà envoyé.")
    }
  } else{
    verificationDoubleColission = false;
  }
  fonction.notifMqtt(`${position.bateau1[0]},${position.bateau1[1]},${position.bateau1[2]},${position.bateau1[3]}:${position.bateau2[0]},${position.bateau2[1]},${position.bateau2[2]},${position.bateau2[3]}:${position.impacte[0]},${capModif[0]},${capModif[1]}:${position.vent}`);
  //console.log(`position => ${position.bateau1[0]},${position.bateau1[1]},${position.bateau1[2]},${position.bateau1[3]}:${position.bateau2[0]},${position.bateau2[1]},${position.bateau2[2]},${position.bateau2[3]}:${position.vent}:${position.impacte[0]}`);
  //console.log(`position => ${position.vent}:${capModif[0]}:${capModif[1]}`);
  

  const message = {
    bateau1: {
      latitude: position.bateau1[0],
      longitude: position.bateau1[1],
      cap: position.bateau1[2]
    },
    bateau2: {
      latitude: position.bateau2[0],
      longitude: position.bateau2[1],
      cap: position.bateau2[2]
    },
    impacte: position.impacte[0],
    vent: position.vent
  };
  res.json({ message });
});


// ---------------------------------------------------------------------------------
//
//                              Rècuperation Simulation
//
// ---------------------------------------------------------------------------------
app.post('/api/message_simulation', (req, res) => {
  // Récupérer les données du corps de la requête
  const { ruleName, capBateau1Input, capBateau2Input, ventInput } = req.body;

  // Faire ce que vous voulez avec les données, par exemple les afficher dans la console
  /*
  console.log('Règle:', ruleName);
  console.log('Cap bateau Rouge:', capBateau1Input);
  console.log('Cap bateau Bleu:', capBateau2Input);
  console.log('Vent:', ventInput);
  */
  // Répondre à la requête avec un message de succès
  res.send('Données de simulation reçues avec succès !');
  fonction.notifSimulation(`${ruleName},${capBateau1Input},${capBateau2Input},${ventInput}`);
});
//  mosquitto_pub -h 192.168.10.105 -t topic -m "46.49,-1.78:46.48,-1.77"
