
function futurPosition(bateau, n){
    let distance = bateau[3] * 1.852 / (111.11 * 3600); // 1 degré latitude = 111,11km, 1 noeud = 1,852km/h = 0,514 m/s
    let latitude = bateau[0];
    let longitude = bateau[1];
    let cap = inverserDegres(bateau[4]);
    for(i=0 ; i<n ; i++){
        let coslat = Math.cos(latitude * Math.PI / 180);
        let coscap = Math.cos(cap * Math.PI / 180);
        let sincap = Math.sin(cap * Math.PI / 180);
        latitude = latitude + (distance * sincap);
        longitude = longitude + (distance * coslat * coscap);
    }
    let futurPosition = [latitude, longitude];
    
    return futurPosition;
}



function verificationColision(bateau1, bateau2){

    distanceLatitude = Math.abs(bateau1[0]-bateau2[0]);
    distanceLongitude = Math.abs(bateau1[1]-bateau2[1]);

    if(Math.abs(distanceLatitude <= 0.0003) && (distanceLongitude <= 0.0003)){
        return true;
    } else{
        return false;  
    }
}

function correctionCap(bateau1, bateau2, vent){         //Correction du cap automatiquement si les bateaux rentrent en collision
    var cap_bateau1 = bateau1[2];
    var cap_bateau2 = bateau2[2];
    var direction = null;
    //console.log('\x1b[33m%s\x1b[0m', 'Cap1 ' + cap_bateau1 + " Cap2 " + cap_bateau2 + " Vent " + vent);

    var difference_de_cap = Math.abs(cap_bateau1-cap_bateau2);
    switch(true) { //Détermination de la règle à utiliser
        case (difference_de_cap <= 5):
            console.log("les bateaux se suivent");
            break;
        case (difference_de_cap > 10 && difference_de_cap < 90):
            break;
    }
    //console.log('\x1b[33m%s\x1b[0m', 'Direction du vent ' + direction);
    if(Math.abs(cap_bateau1 - cap_bateau2) < 90){

    } else {}
    return [0,0];
}

function inverserDegres(valeur) { //Pour ajuster le cap en fonction de la rose des vents
    valeur_inverse = 360 - (valeur - 90);
    if(valeur_inverse < 0 ){
        valeur_inverse = 360 - valeur_inverse;
    }
    return valeur_inverse;
}

module.exports = {
    futurPosition,
    verificationColision,
    correctionCap
};
