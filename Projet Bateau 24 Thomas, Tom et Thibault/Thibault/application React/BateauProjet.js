import React, { useState, useEffect } from 'react';
import { MapContainer, TileLayer, Marker, Popup, ScaleControl} from 'react-leaflet';

import { Button, Navbar, Container } from 'react-bootstrap'; //Bootstrap framework CSS + modal
import 'bootstrap/dist/css/bootstrap.min.css';
import ExampleModal from './ModalSimulation';

import './App.css';

import axios from 'axios'; // Api communication vers node

import 'leaflet-rotatedmarker'; // Map

import warning from './ressources/warning.png'; //Sources des images
import good from './ressources/good.png';
import fleche from './ressources/fleche.png';


import { createBateau1, createBateau2, createBalise } from './fonction';

function BateauProjet() {

  const positionVue = [46.491880220982, -1.7854207668632815];
  const position = [46.502000220982, -1.7854207668632815];
  const position2 = [46.50200220982, -1.7857207668632815];
  var time;
  

  const [positionBateau1, setPosition1] = useState([]);
  const [positionBateau2, setPosition2] = useState([]);
  const [capBateau1, setCap1] = useState(0);
  const [capBateau2, setCap2] = useState(0);
  const [vent, setVent] = useState(0);
  const [impact, setImpact] = useState(false);

  const rotation = `rotate(${vent}deg)`;

  useEffect(() => {  //Début interval
    const intervalId = setInterval(async () => {
      try {
        let currentTimeInSeconds = Math.floor(Date.now() / 1000);
        const response = await axios.get('http://192.168.1.105:3001/api/message');
        let message = response.data.message;
        let impact = message.impacte;
        const positionBateau1 = [message.bateau1.latitude, message.bateau1.longitude];
        const positionBateau2 = [message.bateau2.latitude, message.bateau2.longitude];
        
        setPosition1(positionBateau1);
        setPosition2(positionBateau2);

        setVent(message.vent);
        
        if(time >= currentTimeInSeconds){
          document.body.classList.add('impacte');
          setImpact(true);
        } else{
          if(impact == true){
            time = currentTimeInSeconds+1;
            if(impact) {
              document.body.classList.add('impacte');
              setImpact(true);
            }
          } else{
            document.body.classList.remove('impacte');
            setImpact(false);
          }
          
        }
        console.log("IMPACTE : " + message.impacte + " " + message.vent);
        setCap1(message.bateau1.cap);
        setCap2(message.bateau2.cap);
        
      } catch (error) {
        console.error('Erreur lors de la récupération des données:', error);
      }
    }, 1000);

    

    return () => {
      // Nettoyer l'intervalle lors du démontage du composant
      clearInterval(intervalId);
    };
  }, []); //Fin interval



  return (
    
    <>
      <div className="map">
        <MapContainer center={positionVue} zoom={13} style={{ height: '100%', width: '100%' }}>
          <TileLayer
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
          />
          <ScaleControl />
          <Marker position={[46.4860, -1.7874]} icon={createBalise(0)}> {/* Balise de gauche */}
            <Popup>
              Balise nautique
            </Popup>
          </Marker>
          <Marker position={[46.4860, -1.7804]} icon={createBalise(0)}>{/* Balise de droite */}
            <Popup>
              Balise nautique
            </Popup>
          </Marker>
          {positionBateau1.length === 0 ?
            <Marker position={[46.5040, -1.7854]} icon={createBateau1(0)} className="icone-bateau1">
              <Popup>
                A pretty CSS3 popup. <br /> Easily customizable.
              </Popup>
            </Marker>
            :
            <Marker position={positionBateau1} icon={createBateau1(capBateau1)} className="icone-bateau2">
              <Popup>
                Fonctionnement {capBateau1} <br /> Position : {positionBateau1}
              </Popup>
            </Marker>
          }
          {positionBateau2.length === 0 ?
            <Marker position={[46.5043, -1.7854]} icon={createBateau2(0)} className="icone-bateau1">
              <Popup>
                A pretty CSS3 popup. <br /> Easily customizable.
              </Popup>
            </Marker>
            :
            <Marker position={positionBateau2} icon={createBateau2(capBateau2)} className="icone-bateau2">
              <Popup>
                Fonctionnement : {capBateau2} <br /> Position : {positionBateau2}
              </Popup>
            </Marker>
          }

        </MapContainer>

      </div>
      <div className="warning">
        {impact ? <img src={warning} className="warning_img"></img> : <img src={good} className="good_img"></img>}
      </div>
      
      <div className="fleche">
        {vent === 1 ? 
        <img src={fleche} className='flecheVent' alt="Flèche du vent" />
        : 
        <img src={fleche} className='flecheVent' style={{ transform: rotation }} alt="Flèche du vent" />
        }
      </div>


      <div className='BoutonModal'>
        <ExampleModal />
      </div>
        
      
    </>
  );
}

export default BateauProjet;