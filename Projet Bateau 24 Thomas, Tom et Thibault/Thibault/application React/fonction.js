import L from 'leaflet'; // Assurez-vous d'importer Leaflet (map)
import { MapContainer, TileLayer, Marker, Popup } from 'react-leaflet';
import bateau_rouge from './ressources/bateau_rouge.png';
import bateau_bleu from './ressources/bateau_bleu.png';
import balise from './ressources/balise_nautique.png';

import axios from 'axios'; // Api communication vers node

export function createBateau1(cap) {
    // Créer une icône de bateau avec la rotation en fonction du cap
    const iconUrl = bateau_rouge;
    const iconSize = [36, 72];
    const iconAnchor = [16, 16];
    const rotation = `rotate(${cap}deg)`;

    const icon = L.divIcon({
        className: 'boat-icon',
        html: `<img src="${iconUrl}" style="transform: ${rotation}; width: ${iconSize[0]}px; height: ${iconSize[1]}px;"/>`,
        iconSize: iconSize,
        iconAnchor: iconAnchor,
    });
    return icon;
}

export  function createBateau2(cap) {
    // Créer une icône de bateau avec la rotation en fonction du cap
    const iconUrl = bateau_bleu;
    const iconSize = [36, 72]; // Ajuster la taille de l'icône en fonction du niveau de zoom
    const iconAnchor = [15, 15];
    const rotation = `rotate(${cap}deg)`;

    const icon = L.divIcon({
        className: 'boat-icon',
        html: `<img src="${iconUrl}" style="transform: ${rotation}; width: ${iconSize[0]}px; height: ${iconSize[1]}px;"/>`,
        iconSize: iconSize,
        iconAnchor: iconAnchor,
    });
    return icon;
}

export  function createBalise(cap) {
    // Créer une icône de bateau avec la rotation en fonction du cap
    const iconUrl = balise;
    const iconSize = [30, 30]; // Ajuster la taille de l'icône en fonction du niveau de zoom
    const iconAnchor = [15, 15];
    const rotation = `rotate(${cap}deg)`;

    const icon = L.divIcon({
        className: 'boat-icon',
        html: `<img src="${iconUrl}" style="transform: ${rotation}; width: ${iconSize[0]}px; height: ${iconSize[1]}px;"/>`,
        iconSize: iconSize,
        iconAnchor: iconAnchor,
    });
    return icon;
}

export async function sendDataToServer(ruleName, capBateau1Input, capBateau2Input, ventInput){
    try {
        const response = await axios.post('http://192.168.1.105:3001/api/message_simulation', {
          ruleName: ruleName,
          capBateau1Input: capBateau1Input,
          capBateau2Input: capBateau2Input,
          ventInput: ventInput
        });
        console.log('Données envoyées avec succès:', response.data);
      } catch (error) {
        console.error('Erreur lors de l\'envoi des données:', error);
      }
}
