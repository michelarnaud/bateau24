import React, { useState } from 'react';
import { Modal, Button } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import gear from './ressources/gear_icon.png';


import { sendDataToServer } from './fonction';

function ExampleModal() {

  const [show, setShow] = useState(false);
  const [test1, setTest1] = useState(false);
  const [test2, setTest2] = useState(false);
  const [test3, setTest3] = useState(false);

  const [capBateau1InputR1, setCapBateau1InputR1] = useState(315); //règle tribord amure
  const [capBateau2InputR1, setCapBateau2InputR1] = useState(45);
  const [ventInputR1, setVentInputR1] = useState(180);

  const [capBateau1InputR2, setCapBateau1InputR2] = useState(10); //Règle meme amure
  const [capBateau2InputR2, setCapBateau2InputR2] = useState(55);
  const [ventInputR2, setVentInputR2] = useState(145);

  const [capBateau1InputR3, setCapBateau1InputR3] = useState(270); //Règle meme amure
  const [capBateau2InputR3, setCapBateau2InputR3] = useState(90);
  const [ventInputR3, setVentInputR3] = useState(0);

  const testClick1 = () => {
    setTest1(true);
    console.log(`Tribord Amure ${capBateau1InputR1} ${capBateau2InputR1} ${ventInputR1}`);
    sendDataToServer("tribord amure", capBateau1InputR1, capBateau2InputR1, ventInputR1);

  };
  const testClick2 = () => {
    setTest2(true);
    console.log(`Même Amure ${capBateau1InputR2} ${capBateau2InputR2} ${ventInputR2}`);
    sendDataToServer("meme amure", capBateau1InputR2, capBateau2InputR2, ventInputR2);

  };
  const testClick3 = () => {
    setTest3(true);
    console.log(`Reset${capBateau1InputR3} ${capBateau2InputR3} ${ventInputR3}`);
    sendDataToServer("reset", capBateau1InputR3, capBateau2InputR3, ventInputR3);

  };
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  return (
    <>
      <Button variant="light" onClick={handleShow} className='Gear-Button' style={{ borderRadius: '50%' }}>
        <img src={gear} className='Gear-Icon'></img>
      </Button>

      <Modal show={show} onHide={handleClose} size="lg">
        <Modal.Header closeButton>
          <Modal.Title>Outil de simulation</Modal.Title>
        </Modal.Header>
        <Modal.Body>

        </Modal.Body>
        <div className="table-responsive">
          <table className="table">
            <thead>
              <tr>
                <th>Choix de la règle</th>
                <th>Cap bateau Rouge</th>
                <th>Cap bateau Bleu</th>
                <th>Vent</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>
                  <Button variant="success" onClick={() => testClick1('Tribord Armure')} style={{ whiteSpace: 'nowrap' }}>Tribord Amure</Button>
                </td>
                <td>
                  <input type="text" className="form-control" placeholder='315°' value={capBateau1InputR1} onChange={(e) => setCapBateau1InputR1(e.target.value)} />
                </td>
                <td>
                  <input type="text" className="form-control" placeholder='45°' value={capBateau2InputR1} onChange={(e) => setCapBateau2InputR1(e.target.value)} />
                </td>
                <td>
                  <input type="text" className="form-control" placeholder='0°' value={ventInputR1} onChange={(e) => setVentInputR1(e.target.value)} />
                </td>
              </tr>
              <tr>
                <td>
                  <Button variant="success" onClick={() => testClick2('Même armure')} style={{ whiteSpace: 'nowrap' }}>Même Amure</Button>
                </td>
                <td>
                  <input type="text" className="form-control" placeholder='10°' value={capBateau1InputR2} onChange={(e) => setCapBateau1InputR2(e.target.value)} />
                </td>
                <td>
                  <input type="text" className="form-control" placeholder='55°' value={capBateau2InputR2} onChange={(e) => setCapBateau2InputR2(e.target.value)} />
                </td>
                <td>
                  <input type="text" className="form-control" placeholder='0°' value={ventInputR2} onChange={(e) => setVentInputR2(e.target.value)} />
                </td>
              </tr>
              <tr>
                <td>
                  <Button variant="danger" onClick={() => testClick3('Reset')} style={{ whiteSpace: 'nowrap' }}>Reset</Button>
                </td>
                <td>
                  <input type="text" className="form-control" placeholder='0°' value={capBateau1InputR3} onChange={(e) => setCapBateau1InputR3(e.target.value)} />
                </td>
                <td>
                  <input type="text" className="form-control" placeholder='0°' value={capBateau2InputR3} onChange={(e) => setCapBateau2InputR3(e.target.value)} />
                </td>
                <td>
                  <input type="text" className="form-control" placeholder='0°' value={ventInputR3} onChange={(e) => setVentInputR3(e.target.value)} />
                </td>
              </tr>
              {
                /*
                <tr>
                <th>Tribord amure : </th>
                <th>320</th>
                <th>45</th>
              </tr>
              <tr>
                <th>Même amure : </th>
                <th>30</th>
                <th>40</th>
                <th></th>
              </tr> 
                */ 
              }
              
            </tbody>
          </table>

        </div>

        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Fermer
          </Button>
        </Modal.Footer>
      </Modal>

    </>
  );
}

export default ExampleModal;
