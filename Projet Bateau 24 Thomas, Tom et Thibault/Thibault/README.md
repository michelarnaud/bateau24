
Bonjour toi qui récupère mon sublime code.

Dans ces fichiers tu trouveras 2 dossier un pour le back-end et un pour le front-end:

serveurNode (back end) :

 - server.js : c'est le fichier principale qui sera à lancer il permet de collecter des données et de les envoyer au site web.
 - fonction.js : dans ce fichier tu trouveras pleins de fonction que le server.js utilisera.
 - regles_navigation.js : pour soulager le fichier fonction.js j'ai rassembler les règles de navigations ici qui sont appellé par fonction.js.
 - mysql.js : Ce fichier peut être lancé pour créer la bdd MYSQL aulieu de la créer à la main.

Application React (front-end) :

 - /ressources : Toutes les images utilisé par le site sont présent ici.
 - App.css : Fichier css du site web.
 - App.js : Page principale du site web qui appelle BateauProjet.js .
 - BateauProjet.js : Code principale de la page web.
 - fonction.js : Page de fonction utilisé pour créer des bateaux et d'autres choses.
 - ModalSimulation.js : Permet la création de la modale de simulation en bas à droite de la page web.