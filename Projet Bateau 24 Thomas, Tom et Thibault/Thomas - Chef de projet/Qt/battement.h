#ifndef _BATTEMENT
#define _BATTEMENT
#include <qthread.h>

class Coeur;
class Battement : public QThread
{
  Q_OBJECT

  private :
  volatile bool stopped;
 public :
    Battement(QObject *parent = 0);
    int t;
    void stop();
    void startb();
    void raztemps();
    int liretemps();
  protected :
     virtual void run();
  signals:
   void miseajour();

};


#endif
