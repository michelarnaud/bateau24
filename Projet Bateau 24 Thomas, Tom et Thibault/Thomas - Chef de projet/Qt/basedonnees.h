
#ifndef MYSQLCONNECTOR_H
#define MYSQLCONNECTOR_H

#include <QtSql/QSqlDatabase>
#include <QDebug>

class MysqlConnector
{


    public:
        MysqlConnector();

        bool connect();
        QString execQuery(const QString &query);
        void disconnect();

    private:
        QSqlDatabase db;
};

#endif // MYSQLCONNECTOR_H


