#include "mqtt.h"

MQTT::MQTT()
{
    m_client = new QMqttClient();
    m_client->setPort(1883);

    m_serveur = new QMqttClient();
    m_serveur->setPort(1883);
    m_serveur->setHostname("192.168.10.164");
    m_serveur->connectToHost();
}

MQTT::~MQTT()
{
    delete m_client;
    delete m_serveur;
}

bool MQTT::connectionHote(QString hostname)
{
    m_client->setHostname(hostname);
    if (m_client->state() == QMqttClient::Disconnected) {
        m_client->connectToHost();
    }
    return true;
}

bool MQTT::subscribe()
{
    m_serveur->connectToHost();
    QString topic;
    topic.append("simulation");

    bool subscription = m_serveur->subscribe(topic);
    return subscription;
}

bool MQTT::publish(QByteArray morpion)
{
    QString topic;
    topic.append("topic");

    bool publish = m_client->publish(topic, morpion);
    return publish;
}
