#include "bateau.h"
#include "ui_bateau.h"
#include <cmath>
#include <iostream>
#include <QtMqtt/QMqttClient>
#include <mqtt.h>
#include <chrono>
#include <QDebug>
#include<unistd.h>
#include <QtSql/QSqlQuery>
#include <basedonnees.h>

bateau::bateau(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::bateau)
{

    ui->setupUi(this);
    m_bat=new Battement(this);
    connect(m_bat,SIGNAL(miseajour()),this,SLOT(on_pushButtonPosition_pressed()));
    m_mqtt = new MQTT();
    m_mqtt->connectionHote("192.168.1.105");
    connect(m_mqtt->m_serveur, &QMqttClient::messageReceived, this, [this](const QByteArray &message){

        const QString content = message;
        modificationCap(content);

    });
}


bateau::~bateau()
{
    delete m_mqtt;
    delete ui;
}

void bateau::on_CAP_sliderMoved(int position)
{
    ui->lcdCap->display(position);
    text[0] = position;
}

void bateau::on_Vitesse_sliderMoved(int position)
{
    ui->lcdVitesse->display(position);
    text[0] = position;
}

void bateau::on_CAP_2_sliderMoved(int position)
{
    ui->lcdCap_2->display(position);
    text[0] = position;
}

void bateau::on_Vitesse_2_sliderMoved(int position)
{
    ui->lcdVitesse_2->display(position);
    text[0] = position;
}

void bateau::on_DirectionVent_sliderMoved(int position)
{
    ui->lcdDirectionVent->display(position);
    text[0] = position;
}

void bateau::on_VitesseVent_sliderMoved(int position)
{
    ui->lcdVitesseVent->display(position);
    text[0] = position;
}

void bateau::on_AngleBarre_sliderMoved(int position)
{
    ui->lcdAngleBarre->display(position);
    text[0] = position;
}

void bateau::on_AngleBarre_2_sliderMoved(int position)
{
    ui->lcdAngleBarre_2->display(position);
    text[0] = position;
}

int bateau::inverserDegres(int valeur) {
    int valeurInverse = 360-(valeur-90);

    if (valeurInverse < 0){
        valeurInverse = 360 - valeurInverse;
    }

    return valeurInverse;
}

void bateau::modificationCap(const QString &message) { //reçoit la requête de changement de cap via MQTT
    QStringList parts = message.split(",");
    qDebug() << parts[0] << parts[1].toInt() << parts[2].toInt() << parts[3].toInt(); //règle, cap bateau1, cap bateau2, Direction du vent

    if(parts[0] == "tribord amure" || parts[0] == "meme amure"){
        ui->CAP->setValue(parts[1].toInt());
        ui->CAP_2->setValue(parts[2].toInt());
        ui->DirectionVent->setValue(parts[3].toInt());
        ui->lcdCap->display(parts[1].toInt());
        ui->lcdCap_2->display(parts[2].toInt());
        ui->lcdDirectionVent->display(parts[3].toInt());
        ui->lcdVitesse->display(25);
        ui->lcdVitesse_2->display(25);
        ui->Vitesse->setValue(25);
        ui->Vitesse_2->setValue(25);
        if(parts[0] == "tribord amure"){
            latitude = 46.490225; // Coordonnées du bateau 1
            longitude = -1.78;
            latitude2 = 46.490225; // Coordonnées du bateau 2
            longitude2 = -1.785;
        } else{
            latitude = 46.4896; // Coordonnées du bateau 1
            longitude = -1.78;
            latitude2 = 46.490225; // Coordonnées du bateau 2
            longitude2 = -1.781;
        }
    } else if(parts[0] == "reset"){
        ui->CAP->setValue(parts[1].toInt());
        ui->CAP_2->setValue(parts[2].toInt());
        ui->DirectionVent->setValue(parts[3].toInt());
        ui->Vitesse->setValue(0);
        ui->Vitesse_2->setValue(0);

        // Modification graphique
        ui->lcdCap->display(parts[1].toInt());
        ui->lcdCap_2->display(parts[2].toInt());
        ui->lcdDirectionVent->display(parts[3].toInt());
        ui->lcdVitesse->display(0);
        ui->lcdVitesse_2->display(0);


        latitude = 46.490225; // Coordonnées du bateau 1
        longitude = -1.78;
        latitude2 = 46.490225; // Coordonnées du bateau 2
        longitude2 = -1.785;

    }
}

void bateau::alerteChangementCap(){
    MysqlConnector mysql;

    if (mysql.connect()) {
        QString valeurs = mysql.execQuery("SELECT * FROM collisions"); // Exécuter une requête

        QStringList parts = valeurs.split(":");
        qDebug() << "valeurs : " << parts[0] << " " << parts[1] << " " << parts[2];
        if(parts[0].toInt() == 1){
            ui->CAP->setValue(parts[1].toInt());
            ui->CAP_2->setValue(parts[2].toInt());
            ui->lcdCap->display(parts[1].toInt());
            ui->lcdCap_2->display(parts[2].toInt());
        } else{
            return;
        }


        mysql.disconnect(); // Déconnexion
    } else {
        qDebug() << "Impossible de se connecter à la base de données.";
    }
}

void bateau::on_pushButtonPosition_pressed(){

    alerteChangementCap();

    m_mqtt->subscribe();

   double distance = ui->lcdVitesse->value()*1.852/(111.11*3600); // 1 degré latitude = 111,11km     1 degré longitude = 111,11*cos(degré latitude) | 1 noeud = 1,852km/h = 0,514 m/s

   int cap = inverserDegres(ui->lcdCap->value());

   float coslat=cos(latitude*M_PI/180);
   float coscap=cos (cap*M_PI/180); //  double angleEnRadians = angleEnDegres * M_PI / 180.0;
   float sincap=sin(cap*M_PI/180);
   latitude += distance * sincap;
   longitude += distance * coslat * coscap;

    //qDebug() << "Ajout : " << (distance * sincap) << " " << (distance * coslat * coscap);

   double distance2 = ui->lcdVitesse_2->value()*1.852/(111.11*3600); // Déplacement du second bateau


   int cap2 = inverserDegres(ui->lcdCap_2->value());


   float coslat2=cos(latitude2*M_PI/180);
   float coscap2=cos (cap2*M_PI/180);
   float sincap2=sin(cap2*M_PI/180);
   latitude2 += distance2 * sincap2;
   longitude2 += distance2 * coslat2 * coscap2;



   if (latitude > 90){ //limite de latitude
       latitude = -90;
   }
   else if (latitude < -90)
       latitude = 90;



   if (latitude2 > 90){
       latitude2 = -90;
   }
   else if (latitude2 < -90)
       latitude2 = 90;


   if (longitude > 180 || longitude < -180) {
        // Inverser la longitude en cas de dépassement
       longitude = -longitude;
    }


   if (longitude2 > 180 || longitude2 < -180) {
       longitude2 = -longitude2;
    }


   //Aperçu de l'envoi dans la console
   qDebug() <<latitude << ","<< longitude <<","<< ui->lcdCap->value()<<","<< ui->lcdVitesse->value()<< "|"<< latitude2 << ","<< longitude2 << ","<< ui->lcdCap_2->value()<< ","<<  ui->lcdVitesse_2->value() << "|" << ui->lcdDirectionVent->value() << ","<< ui->lcdVitesseVent->value();

   cap2 = ui->lcdCap_2->value();
   cap = ui->lcdCap->value();





   QString latitudeString = QString::number(latitude); // Convertir la valeur float en QString
   QString longitudeString = QString::number(longitude);
   QString capString = QString::number(cap);
   QString capReelString = QString::number(ui->lcdCap->value());
   QString vitesseString = QString::number(ui->Vitesse->value());

   QString latitudeString2 = QString::number(latitude2);
   QString longitudeString2 = QString::number(longitude2);
   QString cap2String = QString::number(cap2);
   QString capReel2String = QString::number(ui->lcdCap_2->value());
   QString vitesse2String = QString::number(ui->Vitesse_2->value());

   QString directionVentString = QString::number(ui->lcdDirectionVent->value());



   // Concaténer la chaîne d'informations convertie en QString
   QByteArray *message = new QByteArray(latitudeString.toUtf8() + "," + longitudeString.toUtf8()+ "," + capString.toUtf8() + "," + vitesseString.toUtf8() + "," + capReelString.toUtf8() + ":" +latitudeString2.toUtf8() + "," + longitudeString2.toUtf8() + "," + cap2String.toUtf8() + "," + vitesse2String.toUtf8() + "," + capReel2String.toUtf8()+ ":" +directionVentString.toUtf8());
   m_mqtt->publish(*message);
}

