#include "basedonnees.h"
#include <QtSql/QSqlQuery> // Ajout de l'en-tête de QSqlQuery

MysqlConnector::MysqlConnector()
{
    // Configuration de la connexion à la base de données MySQL
    db = QSqlDatabase::addDatabase("QMYSQL");
    db.setHostName("localhost"); // nom de l'hôte
    db.setDatabaseName("Bateau"); // nom de la base de données
    db.setUserName("root"); //nom de l'utilisateur
    db.setPassword("root"); // mot de passe
}

bool MysqlConnector::connect()
{
    // Connexion à la base de données
    if (!db.open()) {
        qDebug() << "Erreur lors de la connexion à la base de données.";
        return false;
    }
    qDebug() << "Connexion réussie à la base de données !";
    return true;
}

QString MysqlConnector::execQuery(const QString &query)
{
    QSqlQuery q(db); // Utilisation de db dans le constructeur de QSqlQuery

    if (!db.isOpen()) { // Vérifiez si la base de données est ouverte
        if (!db.open()) {
            qDebug() << "Erreur lors de l'ouverture de la base de données";
            return "error";
        }
    }

    if (q.exec(query)) {
        while (q.next()) {
            QString detection = q.value("collision_detected").toString();
            QString bateau1 = q.value("bateau1").toString();
            QString bateau2 = q.value("bateau2").toString();
            qDebug() << "bateau1:" << bateau1 << ", bateau2:" << bateau2;
            return (detection + ":" + bateau1 + ":" + bateau2);
        }
    } else {
        qDebug() << "Erreur lors de l'exécution de la requête.";
        return "error";
    }
}

void MysqlConnector::disconnect()
{
    // Fermeture de la connexion
    db.close();
}
